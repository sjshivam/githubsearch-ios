//
//  DataManager.swift
//  GithubSearch
//
//  Created by Shivam Jaiswal on 25/01/19.
//  Copyright © 2019 MM. All rights reserved.
//

import Foundation

class DataManager {
    
    private static let sharedDataManager: DataManager = {
        let dataManager = DataManager()
        return dataManager
    }()
    
    class func shared() -> DataManager {
        return sharedDataManager
    }
    
    func fetchRepositories(keyword: String, page: Int, completion: @escaping (RepositoriesMeta?, Error?) -> Void)
    {
        let parameters: [String : Any] = ["q" : keyword, "sort" : "stars", "order" : "desc", "per_page" : 20, "page" : page]
        NetworkManager.shared().createGETRequest(path: EndPoint.getRepositories, parameters: parameters) { (response, error) in
            
            if let error = error{
                completion(nil, error)
                return
            }
            
            guard let response = response else {
                completion(nil, MMError.defaultError)
                return
            }
            
            print("fetched: \(keyword) page \(page)")
            let repoMeta = RepositoriesMeta.init(data: response)
            completion(repoMeta, nil)
        }
    }
}

