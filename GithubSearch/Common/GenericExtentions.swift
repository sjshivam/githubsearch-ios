//
//  GenericExtentions.swift
//  GithubSearch
//
//  Created by Shivam Jaiswal on 28/01/19.
//  Copyright © 2019 MM. All rights reserved.
//

import Foundation
import UIKit
extension UITableView{
    
    func isLastCell(for indexpath: IndexPath) -> Bool {
        return (self.numberOfRows(inSection: indexpath.section) - 1 ) == indexpath.row
    }
}
