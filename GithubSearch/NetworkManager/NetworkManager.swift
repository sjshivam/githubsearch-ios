//
//  NetworkManager.swift
//  GithubSearch
//
//  Created by Shivam Jaiswal on 25/01/19.
//  Copyright © 2019 MM. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireNetworkActivityIndicator

struct EndPoint {
    private init() {}
    static let getRepositories = "search/repositories"
}

class NetworkManager {
    
    private static let sharedNetworkManager: NetworkManager = {
        NetworkActivityIndicatorManager.shared.isEnabled = true
        let networkManager = NetworkManager()
        return networkManager
    }()
    
    class func shared() -> NetworkManager {
        return sharedNetworkManager
    }
    
    // MARK: - Properties
    private static let baseURL = "https://api.github.com/"
    
    
    // With Alamofire
    func createGETRequest(path: String, parameters: [String : Any], completion : @escaping ([AnyHashable : Any]?, Error? ) -> Void)  {
        
        guard let url = URL(string: NetworkManager.baseURL + path ) else {
            completion(nil, MMError.defaultError)
            return
        }
        
        Alamofire.request(url, method: .get, parameters: parameters)
            .validate()
            .responseJSON { [weak self] response in self?.processResponse(response, completion: completion) }
    }
    
    func createPOSTRequest(path: String, parameters: [String : Any], completion : @escaping ([AnyHashable : Any]?, Error? ) -> Void)  {
        
        guard let url = URL(string: NetworkManager.baseURL + path ) else {
            completion(nil, MMError.defaultError)
            return
        }
        
        Alamofire.request(url, method: .post, parameters: parameters)
            .validate()
            .responseJSON { [weak self] response in self?.processResponse(response, completion: completion) }
    }
    
    private func processResponse(_ response: DataResponse<Any>, completion : @escaping ([AnyHashable : Any]?, Error? ) -> Void)  {
        guard response.result.isSuccess else {
            completion(nil, response.result.error)
            return
        }
        
        guard let responseJSON = response.result.value as? [AnyHashable: Any] else{
            completion(nil, MMError.defaultError)
            return
        }
        completion(responseJSON, nil)
    }
}

enum MMError: Error {
    case defaultError
}

extension MMError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .defaultError:
            return NSLocalizedString("Oops! Something went wrong!!", comment: "")
        }
    }
}
