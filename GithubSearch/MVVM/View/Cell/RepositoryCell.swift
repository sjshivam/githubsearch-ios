//
//  RepositoryCell.swift
//  GithubSearch
//
//  Created by Shivam Jaiswal on 25/01/19.
//  Copyright © 2019 MM. All rights reserved.
//

import UIKit
import AlamofireImage

class RepositoryCell: UITableViewCell {
    static let cellId = "RepositoryCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    func setup(){
        
    }
    
    func update(urlString: String, name: String){
        textLabel?.text = name
        
        guard let url = URL(string: urlString) else{
            imageView?.image = #imageLiteral(resourceName: "placeholder_loading")
            return
        }
        
        imageView?.af_setImage(
            withURL: url,
            placeholderImage: #imageLiteral(resourceName: "placeholder_loading"),
            filter: nil,
            imageTransition: .crossDissolve(0.2)
        )
    }
    
}
