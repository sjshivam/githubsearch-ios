//
//  ViewController.swift
//  GithubSearch
//
//  Created by Shivam Jaiswal on 25/01/19.
//  Copyright © 2019 MM. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: MMTextField!
    
    let viewModel = RepositoriesViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        searchTextField.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(RepositoryCell.self, forCellReuseIdentifier: RepositoryCell.cellId)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchTextField.becomeFirstResponder()
    }
    
    func downloadData(){
        searchTextField.resignFirstResponder()
        showLoader()
        
        viewModel.fetchRepositories{ [weak self] (fetched, error) in
            self?.hideLoader()
            if error != nil{
                print(error ?? "")
            }
            self?.tableView.reloadData()
        }
    }
    
    func showLoader(){
        searchTextField.setTextFiledView(viewType: .loader, position: .right, tapHandler: nil)
    }
        
    func hideLoader(){
        searchTextField.setTextFiledView(viewType: nil, position: .right, tapHandler: nil)
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.height()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: RepositoryCell.cellId, for: indexPath)  as! RepositoryCell
        let item = viewModel.itemAtIndexPath(indexPath)
        cell.update(urlString: item.owner.avatarUrl, name: item.fullName)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView.isLastCell(for: indexPath) && viewModel.shouldFetchMore() {
            viewModel.nextPageSetup()
            downloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = viewModel.itemAtIndexPath(indexPath)
        print("score is \(item.scoreAnalyzer.score())")
    }
}

extension SearchViewController: UITextFieldDelegate
{
    func textFieldDidEndEditing(_ textField: UITextField) {
        viewModel.keyword = textField.text ?? ""
        viewModel.pageReset()
        downloadData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchTextField.endEditing(true)
        return true
    }
}
