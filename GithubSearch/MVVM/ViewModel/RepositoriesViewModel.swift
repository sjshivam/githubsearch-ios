//
//  RepositoriesViewModel.swift
//  GithubSearch
//
//  Created by Shivam Jaiswal on 25/01/19.
//  Copyright © 2019 MM. All rights reserved.
//

import Foundation
import UIKit

class RepositoriesViewModel
{
    private var repositoriesMeta: RepositoriesMeta?
    var keyword = ""
    var page = 1
    
    func fetchRepositories(completion: @escaping (Bool, Error?) -> Void){
        DataManager.shared().fetchRepositories(keyword: keyword, page: page) { [weak self] (repoMeta, error) in
            self?.processResponse(repoMeta: repoMeta, error: error, completion: completion)
        }
    }
    
    private func processResponse(repoMeta: RepositoriesMeta?, error: Error?, completion: @escaping (Bool, Error?) -> Void)
    {
        guard let repoMeta = repoMeta else {
            completion(false, error ?? MMError.defaultError)
            return
        }
        
        if repositoriesMeta == nil{
            repositoriesMeta = repoMeta
        }else{
            repoMeta.appendItems(repositoriesMeta!.items)
            repositoriesMeta = repoMeta
            print("count = \(repoMeta.items.count)")
            repoMeta.items.forEach({ (repo) in
                print(repo.fullName)
            })
        }
        completion(true, nil)
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRows() -> Int {
        return repositoriesMeta?.items.count ?? 0
    }
    
    func itemAtIndexPath(_ indexPath: IndexPath) -> Repositories {
        return repositoriesMeta?.items[indexPath.row] ?? Repositories(data: [:])
    }
    
    func height() -> CGFloat {
        return 88
    }
    
    func nextPageSetup()  {
        page = page + 1
    }
    
    func pageReset(){
        page = 1
        repositoriesMeta = nil
    }
    
    func shouldFetchMore() -> Bool {
        guard let meta = repositoriesMeta else { return false }
        return meta.totalCount > meta.items.count
    }
}
