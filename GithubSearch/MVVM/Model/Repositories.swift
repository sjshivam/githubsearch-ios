//
//  Repo.swift
//  GithubSearch
//
//  Created by Shivam Jaiswal on 25/01/19.
//  Copyright © 2019 MM. All rights reserved.
//

import Foundation

protocol Parser {
    init(data: [AnyHashable : Any])
}

class RepositoriesMeta: Parser {
    let incompleteResults: Bool
    private (set) var items: [Repositories]
    let totalCount : UInt
    
    required init(data: [AnyHashable : Any]) {
        incompleteResults = data["incomplete_results"] as? Bool ?? false
        totalCount = data["total_count"] as? UInt ?? 0
        let rawItems = data["items"] as? [[AnyHashable : Any]] ?? []
        var repos = [Repositories]()
        for item in rawItems {
            let repo = Repositories.init(data: item)
            repos.append(repo)
        }
        self.items = repos
    }
    
    func appendItems(_ items: [Repositories]){
        self.items = items + self.items
    }
}

class Repositories: Parser
{
    let repoId: UInt
    let fullName: String
    let desc: String
    let score: Double
    let owner: Owner
    let scoreAnalyzer: ScoreAnalyzer
    
    required init(data: [AnyHashable : Any]) {
        repoId = data["id"] as? UInt ?? UInt.min
        fullName = data["full_name"] as? String ?? "N/A"
        desc = data["description"] as? String ?? "N/A"
        score = data["score"] as? Double ?? 0
        let rawOwner = data["owner"] as? [AnyHashable : Any] ?? [:]
        self.owner = Owner.init(data: rawOwner)
        scoreAnalyzer = ScoreAnalyzer.init(data: data)
    }
}

class ScoreAnalyzer
{
    let watchersCount: Int
    let deltaTime: TimeInterval
    let stars : Int
    let forks : Int
    let openIssues: Int
    let size: Int
    
    required init(data: [AnyHashable : Any]) {

        stars = data["stargazers_count"] as? Int ?? 0
        watchersCount = data["watchers_count"] as? Int ?? 0
        forks = data["forks_count"] as? Int ?? 0
        openIssues = data["open_issues_count"] as? Int ?? 0
        size = data["size"] as? Int ?? 0
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        if let updatedAt = data["updated_at"] as? String, let u_date = formatter.date(from: updatedAt){
            deltaTime = Date().timeIntervalSince(u_date) / 3600.0
        }else{
            deltaTime = 0
        }
    }
    
    func score() -> Double {
        let numerator = Double(3 * stars + 1 * forks + 1 * watchersCount)
        var denominator = 2 * deltaTime + 1 * Double(openIssues) + 1 * Double(size)
        denominator = denominator == 0 ? 1 : denominator
        return numerator/denominator
    }
}

class Owner: Parser {
    let avatarUrl: String
    required init(data: [AnyHashable : Any]) {
        avatarUrl = data["avatar_url"] as? String ?? ""
    }
}
